-- MySQL dump 10.13  Distrib 5.5.24, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: Assignment
-- ------------------------------------------------------
-- Server version	5.5.24-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Customer`
--

DROP TABLE IF EXISTS `Customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Customer` (
  `customerId` int(11) NOT NULL,
  `customerName` varchar(22) DEFAULT NULL,
  `CustomerAddress` varchar(33) DEFAULT NULL,
  `emailId` varchar(22) DEFAULT NULL,
  PRIMARY KEY (`customerId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Employee`
--

DROP TABLE IF EXISTS `Employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Employee` (
  `employeeId` int(11) NOT NULL DEFAULT '0',
  `employeeName` varchar(22) DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `work` varchar(23) DEFAULT NULL,
  PRIMARY KEY (`employeeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ItemMapStock`
--

DROP TABLE IF EXISTS `ItemMapStock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ItemMapStock` (
  `stockId` int(11) DEFAULT NULL,
  `itemId` int(11) DEFAULT NULL,
  KEY `stockId` (`stockId`),
  KEY `itemId` (`itemId`),
  CONSTRAINT `ItemMapStock_ibfk_1` FOREIGN KEY (`stockId`) REFERENCES `StockItems` (`stockId`),
  CONSTRAINT `ItemMapStock_ibfk_2` FOREIGN KEY (`itemId`) REFERENCES `Items` (`itemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Items`
--

DROP TABLE IF EXISTS `Items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Items` (
  `itemId` int(11) NOT NULL,
  `itemName` varchar(21) DEFAULT NULL,
  `price` decimal(6,2) DEFAULT NULL,
  `category` varchar(22) DEFAULT NULL,
  `quantity` varchar(21) DEFAULT NULL,
  PRIMARY KEY (`itemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Orders`
--

DROP TABLE IF EXISTS `Orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Orders` (
  `employeeId` int(11) DEFAULT NULL,
  `CustomerId` int(11) DEFAULT NULL,
  `deliveryStatus` varchar(21) DEFAULT NULL,
  `orderTime` varchar(32) DEFAULT NULL,
  `category` varchar(21) DEFAULT NULL,
  `noOfItems` int(11) DEFAULT NULL,
  `totalAmount` decimal(6,2) DEFAULT NULL,
  `orderId` int(11) NOT NULL,
  PRIMARY KEY (`orderId`),
  KEY `employeeId` (`employeeId`),
  KEY `CustomerId` (`CustomerId`),
  CONSTRAINT `Orders_ibfk_2` FOREIGN KEY (`CustomerId`) REFERENCES `Customer` (`customerId`),
  CONSTRAINT `Orders_ibfk_1` FOREIGN KEY (`employeeId`) REFERENCES `Employee` (`employeeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OrdersMapItems`
--

DROP TABLE IF EXISTS `OrdersMapItems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OrdersMapItems` (
  `orderId` int(11) DEFAULT NULL,
  `itemId` int(11) DEFAULT NULL,
  KEY `orderId` (`orderId`),
  KEY `itemId` (`itemId`),
  CONSTRAINT `OrdersMapItems_ibfk_1` FOREIGN KEY (`orderId`) REFERENCES `Orders` (`orderId`),
  CONSTRAINT `OrdersMapItems_ibfk_2` FOREIGN KEY (`itemId`) REFERENCES `Items` (`itemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `StockItems`
--

DROP TABLE IF EXISTS `StockItems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StockItems` (
  `stockId` int(11) NOT NULL,
  `currentInventory` varchar(22) DEFAULT NULL,
  `stockDemand` varchar(22) DEFAULT NULL,
  PRIMARY KEY (`stockId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Vendor`
--

DROP TABLE IF EXISTS `Vendor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Vendor` (
  `id` int(11) NOT NULL,
  `name` varchar(21) DEFAULT NULL,
  `address` varchar(33) DEFAULT NULL,
  `PhoneNo` int(14) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `VendorMapStocks`
--

DROP TABLE IF EXISTS `VendorMapStocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VendorMapStocks` (
  `vendorId` int(11) DEFAULT NULL,
  `stockId` int(11) DEFAULT NULL,
  KEY `vendorId` (`vendorId`),
  KEY `stockId` (`stockId`),
  CONSTRAINT `VendorMapStocks_ibfk_1` FOREIGN KEY (`vendorId`) REFERENCES `Vendor` (`id`),
  CONSTRAINT `VendorMapStocks_ibfk_2` FOREIGN KEY (`stockId`) REFERENCES `StockItems` (`stockId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `employeeMapcustomer`
--

DROP TABLE IF EXISTS `employeeMapcustomer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employeeMapcustomer` (
  `employeeId` int(11) DEFAULT NULL,
  `customerId` int(11) DEFAULT NULL,
  KEY `customerId` (`customerId`),
  KEY `employeeId` (`employeeId`),
  CONSTRAINT `employeeMapcustomer_ibfk_1` FOREIGN KEY (`customerId`) REFERENCES `Customer` (`customerId`),
  CONSTRAINT `employeeMapcustomer_ibfk_2` FOREIGN KEY (`employeeId`) REFERENCES `Employee` (`employeeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-08-28 10:36:18
