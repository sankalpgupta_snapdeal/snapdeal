-- MySQL dump 10.13  Distrib 5.5.24, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: restaurant
-- ------------------------------------------------------
-- Server version	5.5.24-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Branch`
--

DROP TABLE IF EXISTS `Branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Branch` (
  `name` varchar(20) NOT NULL,
  `location` varchar(20) DEFAULT NULL,
  `managerId` int(10) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `managerId` (`managerId`),
  CONSTRAINT `Branch_ibfk_1` FOREIGN KEY (`managerId`) REFERENCES `employee` (`eid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Branch`
--

LOCK TABLES `Branch` WRITE;
/*!40000 ALTER TABLE `Branch` DISABLE KEYS */;
INSERT INTO `Branch` VALUES ('royal','delhi',1005),('welcome','kanpur',1001);
/*!40000 ALTER TABLE `Branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BranchPhoneNumbers`
--

DROP TABLE IF EXISTS `BranchPhoneNumbers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BranchPhoneNumbers` (
  `name` varchar(20) NOT NULL,
  `phoneNumber` char(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`phoneNumber`),
  KEY `BranchPhoneNumbers_ibfk_1` (`name`),
  CONSTRAINT `BranchPhoneNumbers_ibfk_1` FOREIGN KEY (`name`) REFERENCES `Branch` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BranchPhoneNumbers`
--

LOCK TABLES `BranchPhoneNumbers` WRITE;
/*!40000 ALTER TABLE `BranchPhoneNumbers` DISABLE KEYS */;
INSERT INTO `BranchPhoneNumbers` VALUES ('royal','34567'),('royal','78568'),('welcome','12345'),('welcome','98765');
/*!40000 ALTER TABLE `BranchPhoneNumbers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Menu`
--

DROP TABLE IF EXISTS `Menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Menu` (
  `itemId` int(5) NOT NULL,
  `itemType` enum('prepared','readymade') DEFAULT NULL,
  `price` decimal(7,2) DEFAULT NULL,
  PRIMARY KEY (`itemId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Menu`
--

LOCK TABLES `Menu` WRITE;
/*!40000 ALTER TABLE `Menu` DISABLE KEYS */;
INSERT INTO `Menu` VALUES (111,'prepared',50.00),(112,'prepared',40.00),(113,'prepared',60.00),(114,'readymade',5.00),(115,'readymade',12.00);
/*!40000 ALTER TABLE `Menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `customerId` int(10) NOT NULL DEFAULT '0',
  `customerName` varchar(20) DEFAULT NULL,
  `customerPhone` char(10) DEFAULT NULL,
  `customerAddress` text,
  `lastpurchasedvalue` decimal(7,2) DEFAULT '0.00',
  `customerCity` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`customerId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'venu','8595380619','okhla',0.00,'delhi'),(2,'madhu','9966002248','kalkaji',0.00,'delhi'),(3,'kiran','9985000345','vaisali',0.00,'kanpur');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `eid` int(10) NOT NULL,
  `ename` varchar(20) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `phone` char(10) DEFAULT NULL,
  `salary` decimal(7,2) DEFAULT NULL,
  `address` text,
  `managerId` int(10) DEFAULT NULL,
  `employeeType` enum('waitor','manager','cook','cashier','delivery boy') DEFAULT NULL,
  PRIMARY KEY (`eid`),
  KEY `managerId` (`managerId`),
  CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`managerId`) REFERENCES `employee` (`eid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1001,'aravind','2011-01-12','9999999999',20000.00,'kanpur',NULL,'manager'),(1002,'balu','2011-02-10','8888888888',15000.00,'kanpur',1001,'waitor'),(1003,'charan','0000-00-00','7777777777',15000.00,' F-131, kanpur',1001,'cook'),(1004,'Dinesh','2008-12-23','6666666666',25000.00,'kanpur',1001,'cashier'),(1005,'Emmanual','2008-01-12','5555555555',25000.00,'delhi',NULL,'manager'),(1006,'federer','2009-08-08','4444444444',15000.00,'delhi',1005,'waitor'),(1007,'gargh','2010-09-13','3333333333',20000.00,'delhi',1005,'waitor'),(1008,'irfan','2011-03-12','2222222222',30000.00,'delhi',1005,'cook');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `orderId` int(10) NOT NULL,
  `cusId` int(10) DEFAULT NULL,
  `waiterId` int(10) DEFAULT NULL,
  `orderDate` date DEFAULT NULL,
  PRIMARY KEY (`orderId`),
  KEY `waiterId` (`waiterId`),
  KEY `cusId` (`cusId`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`waiterId`) REFERENCES `employee` (`eid`),
  CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`cusId`) REFERENCES `customers` (`customerId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (222,1,1002,'2012-08-24'),(223,2,1006,'2012-08-25'),(224,1,1006,'2012-08-24'),(225,3,1007,'2012-08-24');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suborders`
--

DROP TABLE IF EXISTS `suborders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suborders` (
  `suborderId` int(5) NOT NULL AUTO_INCREMENT,
  `orderId` int(10) DEFAULT NULL,
  `itemId` int(5) DEFAULT NULL,
  `quantity` int(5) DEFAULT NULL,
  PRIMARY KEY (`suborderId`),
  KEY `orderId` (`orderId`),
  KEY `itemId` (`itemId`),
  CONSTRAINT `suborders_ibfk_1` FOREIGN KEY (`orderId`) REFERENCES `orders` (`orderId`),
  CONSTRAINT `suborders_ibfk_2` FOREIGN KEY (`itemId`) REFERENCES `Menu` (`itemId`)
) ENGINE=InnoDB AUTO_INCREMENT=315 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suborders`
--

LOCK TABLES `suborders` WRITE;
/*!40000 ALTER TABLE `suborders` DISABLE KEYS */;
INSERT INTO `suborders` VALUES (219,222,115,1),(220,222,112,2),(221,222,111,3),(222,223,113,5),(223,223,114,1),(311,224,114,1),(312,224,115,2),(313,225,112,2),(314,225,114,3);
/*!40000 ALTER TABLE `suborders` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-08-27 18:16:52
